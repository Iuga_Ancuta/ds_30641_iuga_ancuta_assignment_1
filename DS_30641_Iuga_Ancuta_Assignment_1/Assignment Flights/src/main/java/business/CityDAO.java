package business;

import dataacces.City;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class CityDAO {
    private SessionFactory factory;

    public CityDAO() {
        factory = new Configuration().configure().buildSessionFactory();
    }

    @SuppressWarnings("unchecked")
    public City findCity(String cityName) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE cityName = :cityName");
            query.setParameter("cityName", cityName);
            cities = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        }

        return cities != null && !cities.isEmpty() ? cities.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public List<City> readCities(){
        Session session = factory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try {
            tx = session.beginTransaction();
            cities = session.createQuery("FROM City").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        }
        return cities;
    }
    ;
}
