package business;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;

public class TimeZoneParser {
    public static String parser(String xmlString, String parseBy) {
        String parsedString = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputStream input = new ByteArrayInputStream(xmlString.getBytes());
            Document   document = (Document) builder.parse(input);
            parsedString = document.getElementsByTagName(parseBy).item(0).getTextContent();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return parsedString;
    }
}
