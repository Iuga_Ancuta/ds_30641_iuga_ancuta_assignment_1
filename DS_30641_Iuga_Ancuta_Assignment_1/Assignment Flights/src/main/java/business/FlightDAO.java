package business;

import dataacces.Flight;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class FlightDAO {
    private SessionFactory factory;

    public FlightDAO() {
        factory = new Configuration().configure().buildSessionFactory();
    }

    public Flight addFlight(Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        }
        return flight;
    }
    @SuppressWarnings("unchecked")
    public List<Flight> readFlights(){
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights =null;
        try{
            tx=session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();
            tx.commit();
        } catch(HibernateException e){
            if (tx != null){
                tx.rollback();
            }
        }
        return flights;
    }

    public Flight findFlight(int flightNumber){
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("From Flight WHERE flightNumber = :flightNumber");
            query.setParameter("flightNumber", flightNumber);
            flights = query.list();
            tx.commit();
        } catch(HibernateException e){
            if (tx != null){
                tx.rollback();
            }
        }finally {
        if (session.isOpen()){
            session.close();
        }
    }

        return flights != null && !flights.isEmpty() ? flights.get(0):null;
    }


    public Flight updateFlight(Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
        if (session.isOpen()){
            session.close();
        }
    }

        return flight;
    }
    public Flight deleteFlight(Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            if (session.isOpen()){
                session.close();
            }
        }
        return flight;
    }

    public Flight findFlightT(int flightNumber){
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("From Flight WHERE flightNumber = :flightNumber");
            query.setParameter("flightNumber", flightNumber);
            flights = query.list();
            tx.commit();
        } catch(HibernateException e){
            if (tx != null){
                tx.rollback();
            }
        }

        return flights != null && !flights.isEmpty() ? flights.get(0):null;
    }
}
