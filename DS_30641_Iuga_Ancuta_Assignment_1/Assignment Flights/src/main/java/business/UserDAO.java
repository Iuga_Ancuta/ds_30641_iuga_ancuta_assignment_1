package business;

import dataacces.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class UserDAO {
    private SessionFactory factory;

    public UserDAO(){
        factory = new Configuration().configure().buildSessionFactory();
    }

    public int verifyUser ( String username , String password ){
        Session session = factory.openSession();
        Transaction tx=null;
        List<User> users = null;
        try {
            tx= session.beginTransaction();
            users = session.createQuery("FROM User").list();
            tx.commit();
            for (Object db : users){
                User u =(User) db;
                if (u.getUsername().equals(username) && u.getPassword().equals(password)){
                    if(u.isUserType() == true){
                        return 1;
                    }
                    else{
                        return 2;
                    }
                }
            }
        }catch(HibernateException e){
                if(tx !=null){
                    tx.rollback();
                }
            }
            return 0;
    }
    public User addUser(User user){
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(user);
            tx.commit();
        } catch (HibernateException e){
            if (tx != null){
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return user;
    }

}
