package business;

import java.net.*;
import java.io.*;

public class LocalTime {
    public static String getLocalTime(double latitude, double longitude) {
        String request = "http://api.geonames.org/timezone?lat=" + String.valueOf(latitude) + "&lng=" + String.valueOf(longitude) + "&username=ancaiuga";

        StringBuilder  stringBuilder = null;
        try {
            URL timezone = new URL(request);
            URLConnection con = timezone.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));

            String inputLine = null;
            stringBuilder = new StringBuilder();
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String stringXML = stringBuilder.toString();
        String localtime = TimeZoneParser.parser(stringXML, "time");
        return localtime;
    }
}
