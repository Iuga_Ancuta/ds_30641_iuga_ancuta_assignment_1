package presentation;

import javax.servlet.http.HttpServlet;

import business.FlightDAO;
import business.LocalTime;
import dataacces.*;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

import static business.LocalTime.getLocalTime;

public class UserServlet  extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = null;
        cookies = request.getCookies();
        if (cookies != null) {
            String name = cookies[0].getValue();
            if (!name.equals("") || name != null) {
                response.sendRedirect("/client.html");
            } else {
                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
                out.println("Invalid access");
            }
        }

    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String reads = request.getParameter("bRead");
        String search = request.getParameter("bSearch");
        String flightsNumber = request.getParameter("flightNumber");

        if(reads != null){
            response.sendRedirect("/flights.html");
        }

        if(search != null){
            FlightDAO flights = new FlightDAO();
            Flight f = flights.findFlightT(Integer.parseInt(flightsNumber));
            if(f !=null) {
                City c1 = f.getDepartureCity();
                double lat1 = c1.getLatitude();
                double log1 = c1.getLongitude();
                City c2 = f.getArrivalCity();
                double lat2 = c2.getLatitude();
                double log2 = c2.getLongitude();
                LocalTime l1 = new LocalTime();
                String ltime1 = getLocalTime(lat1, log1);
                LocalTime l2 = new LocalTime();
                String ltime2 = l2.getLocalTime(lat2, log2);

                response.setContentType("text/html");
                PrintWriter out = response.getWriter();

                out.println("<p align='center'>");
                out.println("<TABLE  border=1 style='  border-collapse: collapse; background-color: #f1f1c1;'");
                out.println("<h1>" + "Local time of the flight" + "</h1>");
                out.println("<tr>");
                out.println("<th>" + "Flight number" + "</th>");
                out.println("<th>" + "Airplane type" + "</th>");
                out.println("<th>" + "Departure City" + "</th>");
                out.println("<th>" + "Departure date " + "</th>");
                out.println("<th>" + "Local date " + "</th>");
                out.println("<th>" + "Arrival City" + "</th>");
                out.println("<th>" + "Arrival date " + "</th>");
                out.println("<th>" + "Local date " + "</th>");
                out.println("</tr>");


                    out.println("<tr>");
                    out.println("<td>" + f.getFlightNumber() + "</td>");
                    out.println("<td>" + f.getAirplaneType() + "</td>");
                    out.println("<td>" + c1.getCityName() + "</td>");
                    out.println("<td>" + ltime1 + "</td>");
                    out.println("<td>" + f.getDepartureDate() + "</td>");
                    out.println("<td>" + c2.getCityName() + "</td>");
                    out.println("<td>" + f.getArrivalDate() + "</td>");
                    out.println("<td>" + ltime2 + "</td>");
                    out.println("</tr>");


                out.println("</TABLE>");
                out.println("</p");

            }

        }


    }
}