package presentation;

import javax.servlet.http.HttpServlet;

import dataacces.City;
import dataacces.Flight;
import business.FlightDAO;
import business.CityDAO;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;

public class AdminServlet  extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = null;
        cookies = request.getCookies();
        if (cookies != null) {
            String name = cookies[0].getValue();
            if (!name.equals("") || name != null) {
                response.sendRedirect("/admin.html");
            } else {
                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
                out.println("Invalid acces");
            }
        }

    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String airplaneType = request.getParameter("airplaneType");
        String departureCity =request.getParameter("dCity");
        String departureDate =request.getParameter("dDate");
        String arrivalCity =request.getParameter("aCity");
        String arrivalDate =request.getParameter("aDate");
        String flightNumber =request.getParameter("flightNumber");


        String create = request.getParameter("bCreate");
        String reads = request.getParameter("bReads");
        String update = request.getParameter("bUpdate");
        String delete = request.getParameter("bDelete");

        if(create != null){
            FlightDAO flights  = new FlightDAO();

            CityDAO cities = new CityDAO();
            City c1 = null;
            City c2 = null;
            c1 = cities.findCity(departureCity);
            c2 = cities.findCity(arrivalCity);
            Date ddate=null;
            Date adate=null;
            SimpleDateFormat dt = new SimpleDateFormat("dd-mm-yyyy,hh:mm:ss");
            try {
                 ddate = dt.parse(departureDate);
                 adate = dt.parse(arrivalDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if((c1 != null )&& (c2 !=null)){
                Flight f = new Flight(airplaneType,c1,ddate,c2,adate);
                flights.addFlight(f);
            }
            response.sendRedirect("/flights.html");
        }
        if(reads != null){
            response.sendRedirect("/flights.html");
        }

        if(update != null){
            FlightDAO flights  = new FlightDAO();
            CityDAO cities = new CityDAO();

            City c1 = null;
            City c2 = null;
            c1 = cities.findCity(departureCity);
            c2 = cities.findCity(arrivalCity);

            Date ddate=null;
            Date adate=null;
            SimpleDateFormat dt = new SimpleDateFormat("dd-mm-yyyy,hh:mm:ss");

            Flight f = flights.findFlight(Integer.parseInt(flightNumber));
            try {
                ddate = dt.parse(departureDate);
                adate = dt.parse(arrivalDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if((c1 != null )&& (c2 !=null)){
                f.setAirplaneType(airplaneType);
                f.setArrivalCity(c1);
                f.setDepartureDate(ddate);
                f.setArrivalCity(c2);
                f.setArrivalDate(adate);
                flights.updateFlight(f);
            }
            response.sendRedirect("/flights.html");
        }

        if(delete != null){
            FlightDAO flights  = new FlightDAO();
            Flight f = flights.findFlight(Integer.parseInt(flightNumber));
            if(f != null){
                flights.deleteFlight(f);
            }
            response.sendRedirect("/flights.html");
        }

    }
}