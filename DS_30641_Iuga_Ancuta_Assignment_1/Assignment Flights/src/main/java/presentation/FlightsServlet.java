package presentation;
import dataacces.City;
import dataacces.Flight;
import business.FlightDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


public class FlightsServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        PrintWriter out = response.getWriter();

        FlightDAO db = new FlightDAO();
        List<Flight> l = db.readFlights();


        out.println("<html>");
        out.println("<head>");
        out.println("<title>Flights Page</title>");
        out.println("</head>");
        out.println("<body>");

        out.println("<p align='center'>");
        out.println("<TABLE  border=1 style='  border-collapse: collapse; background-color: #f1f1c1;'");
        out.println("<h1>" + "Flights Schedule" + "</h1>");
        out.println("<tr>");
        out.println("<th>" + "Flight number" + "</th>");
        out.println("<th>" + "Airplane type" + "</th>");
        out.println("<th>" + "Departure City" + "</th>");
        out.println("<th>" + "Departure date " + "</th>");
        out.println("<th>" + "Arrival City" + "</th>");
        out.println("<th>" + "Arrival date " + "</th>");
        out.println("</tr>");

        for (Flight f : l)
        {
            City c = f.getDepartureCity();
            City c1 = f.getArrivalCity();
            out.println("<tr>");
            out.println("<td>" + f.getFlightNumber() + "</td>");
            out.println("<td>" + f.getAirplaneType() + "</td>");
            out.println("<td>" + c.getCityName() + "</td>");
            out.println("<td>" + f.getDepartureDate() + "</td>");
            out.println("<td>" + c1.getCityName() + "</td>");
            out.println("<td>" + f.getArrivalDate() + "</td>");
            out.println("</tr>");
        }

        out.println("</TABLE>");
        out.println("</p");
    }
}
