package presentation;

import business.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;



public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();

        UserDAO db = new UserDAO();

       int ok = db.verifyUser(username,password);
       if( ok == 1){
           response.sendRedirect("/admin");
           String cookieText= username + "_" + password;
           Cookie ck=new Cookie("admin",cookieText);
           ck.setMaxAge(60*60*24);
           response.addCookie(ck);
       }
       if( ok == 2 ) {
           response.sendRedirect("/client");
           String cookieText= username + "_" + password;
           Cookie ck=new Cookie("client",cookieText);
           ck.setMaxAge(60*60*24);
           response.addCookie(ck);
       }
        if(ok == 0){
           response.setContentType("text/html");
           PrintWriter out = response.getWriter();
           out.println("Invalid username or password !");
        }


    }


}
