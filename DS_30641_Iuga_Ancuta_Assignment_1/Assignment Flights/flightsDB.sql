CREATE DATABASE IF NOT EXISTS  flightsdb;
USE flightsdb;

CREATE TABLE IF NOT EXISTS  user
( idUser int primary key auto_increment,
firstName varchar(45),
lastName varchar(45),
username varchar(45),
email varchar(45),
password varchar(45),
userType boolean);

CREATE TABLE IF NOT EXISTS city
(idCity int primary key auto_increment,
cityName varchar(45),
latitude double,
longitude double) ;

CREATE TABLE IF NOT EXISTS flight
( flightNumber int primary key auto_increment ,
airplaneType varchar(45),
departureCity int,
departureDate datetime,
arrivalCity int,
arrivalDate datetime,
foreign key (departureCity) references city (idCity),
foreign key (arrivalCity) references city (idCity));
